import { createMuiTheme, ThemeProvider } from "@material-ui/core";
import getConfig from "next/config";
import Head from "next/head";
import "../../styles/globals.css";

const App = function ({ Component, pageProps }) {
  const { publicRuntimeConfig } = getConfig();
  const PREFIX = publicRuntimeConfig.prefix;
  const theme = createMuiTheme();

  return (
    <>
      <Head>
        <title>ASCII Art Webcam</title>
        <link rel="icon" href={`${PREFIX}/favicon.png`} />
        <meta name="viewport" content="width=device-width, user-scalable=no" />
      </Head>
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </>
  );
};

export default App;
