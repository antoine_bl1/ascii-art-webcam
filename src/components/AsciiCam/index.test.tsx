import React from "react";

import { render } from "react-dom";
import { act } from "react-dom/test-utils";

import AsciiCam from "./index";

describe("AsciiCam", () => {
  it("The video.play() should be called if button is pressed", () => {
    // @ts-ignore
    window.HTMLMediaElement.prototype.play = () => {
      /* do nothing */
    };
    const container = document.createElement("div");
    document.body.appendChild(container);
    act(() => {
      render(<AsciiCam />, container);
    });
    const asciiCam = container.querySelector("video");
    // TODO and mock the .play() on <video />
    const expectedNumber = 10;
    const expectNumber = 1;
    expect(expectNumber).toEqual(expectedNumber);
  });
});
